fun main(){
    println("Please choose the exercise you want to open:")
    val a:Int = readLine()!!.toInt()
        when (a) {
            1 -> bai1()
            2 -> bai2()
            3 -> bai3()
            4 -> bai4()
            5 -> bai5()
            else -> {
                print("Invalid value!")
            }
        }
}

fun bai1(){
    for ( i in 10..200 )
        if(i%7==0 && i%5!=0) {
            print(i)
            print(",")
        }
}

fun bai2(){
    println("Please enter 2 numbers")
    print("First number : ")
    var a: Int = readLine()!!.toInt()
    print("Second number :")
    var b: Int = readLine()!!.toInt()
    println(a.toString(2))
    println(b.toString(16))
}

fun bai3(){
    var array = intArrayOf(14, 15, 1, 7, 6)
    array.sort()
    for (element in array) {
        println(element)
    }

}

fun bai4(){
    println("Type your string: ")
    var inputString: String = readLine()!!
    val strArray = inputString.split(" ".toRegex()).toTypedArray()
    var count = 0

    for (s in strArray) {
        if (s != "") {
            count++
        }
    }
    println(count)
    fun String.capitalizeWords(): String = split(" ").map { it.capitalize() }.joinToString(" ")
    println(inputString.capitalizeWords())


}

fun bai5(){
    print("Input month: ")
    var a: Int = readLine()!!.toInt()
    print("Input year: ")
    var b: Int = readLine()!!.toInt()

    if(a==1 || a==3 || a==5 || a==7 || a==8 || a==10 || a==12)
    {
        print("There are 31 days in the specified month and year")
    }
    else if(a==4 || a==6 || a==9 || a==11)
    {
        print("There are 30 days in the specified month and year")
    }
    else if(a==2)
    {
        if(b%4==0){
            print("There are 29 days in the specified month and year")
        }
        else{
            print("There are 28 days in the specified month and year")
        }
    }
}