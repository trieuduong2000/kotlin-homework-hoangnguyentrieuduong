class HostConfig() {
    var ip:String = ""
    var port: Int = 0
    var type: TypeConnection = TypeConnection.ANY
    constructor(ip: String,port:Int,type:TypeConnection) : this() {
        this.ip = ip
        this.port = port
        this.type = type
    }
}