fun main(){
    val list: MutableList<HostConfig> = mutableListOf()
    print("Enter size of list HostConfig: ")
    var size = readLine()!!.toInt()
    do{
        if (size<4) {
            println("Size must equal or more than 4!\nEnter again: ")
            size = readLine()!!.toInt()
        }
        else if (size>=4) break
        else {
            println("Invalid input.\nEnter again: ")
            size = readLine()!!.toInt()
        }
    }while(true)
    inputList(list,size)
    list.forEach(){
        println("Ip: ${it.ip} Port: ${it.port}  Type: ${it.type}")
    }
    println("List after sort by type")
    sortList(list)
    println("---------------------")
    var flag: String
    filterList(list)
    do{
        println("Do you want to use FILTER again? Say Y/N :")
        flag = readLine()!!
        if(flag == "N") break
        else if(flag == "Y") filterList(list)
        else {
            println("Invalid input.")
        }
    }while(true)

}

fun inputList(list: MutableList<HostConfig>,sizeL:Int){
    var count = 0
    while(count<sizeL){
        list.add(inputInfo(count))
        count++
    }
}

fun inputInfo(additive:Int):HostConfig{
    var counter = additive+1
    print("---HostConfig #$counter---\nEnter Ip:")
    var ip = readLine()!!
    var port: Int = 8888 + additive
    println("Port Default: $port")
    println("1.TypeConnection.ANY\n2.TypeConnection.WIFI\n3.TypeConnection.DATA_4G")
    print("Choose your type:")
    var typeInt = readLine()!!.toInt()
    var type: TypeConnection = TypeConnection.ANY
    when(typeInt){
        1 -> type=TypeConnection.ANY
        2 -> type=TypeConnection.WIFI
        3 -> type=TypeConnection.DATA_4G
    }
    var host = HostConfig(ip,port,type)
    return host
}
fun sortList(lst: MutableList<HostConfig>){
    lst.sortBy { it.type }
    lst.forEach(){
        println("Ip: ${it.ip} Port: ${it.port}  Type: ${it.type}")
    }
}

fun filterList(lst: MutableList<HostConfig>){
    println("1.IP\n2.Port\n3.Type connection\n")
    println("Choose filter conditions:")
    var choose = readLine()!!.toInt()
    when(choose){
        1 -> filterListByIp(lst)
        2 -> filterListByPort(lst)
        3 -> filterListByType(lst)
    }
}
fun filterListByIp(lst: MutableList<HostConfig>){
    print("Insert IP that needed to be found: ")
    var ip: String = readLine()!!
    lst.forEach(){
        if(it.ip.startsWith(ip)){
            println("Ip: ${it.ip} Port: ${it.port}  Type: ${it.type}")
        }
    }
}
fun filterListByPort(lst: MutableList<HostConfig>){
    print("Insert Port that needed to be found: ")
    var port: String = readLine()!!
    lst.forEach(){
        if(it.port.toString().startsWith(port)){
            println("Ip: ${it.ip} Port: ${it.port}  Type: ${it.type}")
        }
    }
}
fun filterListByType(lst: MutableList<HostConfig>){
    println("Type Connection Filter By Type")
    println("Enter Type You Want Choose(ANY,WIFI,DATA_4G):")
    var text : String = readLine()!!
    lst.forEach(){
        if(it.type.toString() == "$text"){
            println("Ip: ${it.ip} Port: ${it.port}  Type: ${it.type}")
        }
    }
}