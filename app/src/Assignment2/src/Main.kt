fun Int.toHex(): String{
    return Integer.toHexString(this)
}

fun String.toBinary(): String{
    var num = Integer.parseInt(this, 16)
    return Integer.toBinaryString(num)
}

fun main(){
    val hexStr = 200.toHex()
    println(hexStr)

    val result = "C8".toBinary()
    println(result)

    var i = 1
    val n = 20
    var t1 = 0
    var t2 = 1

    print("First $n terms: ")

    while (i <= n) {
        print("$t1 + ")

        val sum = t1 + t2
        t1 = t2
        t2 = sum

        i++
    }
}

