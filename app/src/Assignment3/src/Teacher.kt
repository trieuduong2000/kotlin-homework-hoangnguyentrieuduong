open class Teacher : Person() {
    var teaching: String=""
    var income: Long=0
    var teachingHours: Long=0

    override fun inputInfo() {
        print("Enter the class that the teacher is teaching: ")
        teaching = readLine()!!
        do {
            if (!teaching.startsWith("G")
                && !teaching.startsWith("H")
                && !teaching.startsWith("I")
                && !teaching.startsWith("K")
                && !teaching.startsWith("L")
                && !teaching.startsWith("M")) {
                print("Invalid class type. It must begin with G,H,I,K,L,M: ")
                teaching = readLine()!!
            }
            else
            {
                break
            }
        }while(true)
        print("Enter the income per hour of the teacher: ")
        income = readLine()!!.toLong()
        do{
            if(income>0)
            {
                break
            }
            else
            {
                print("Income cannot be negative. Please re-enter: ")
                income = readLine()!!.toLong()
            }
        }while(true)
        print("Enter number of hours that teacher has to teach: ")
        teachingHours = readLine()!!.toLong()
        do{
            if(teachingHours>0)
            {
                break
            }
            else
            {
                print("Teaching hours cannot be negative. Please re-enter: ")
                teachingHours = readLine()!!.toLong()
            }
        }while(true)
    }

    fun incomeCalculator(): Long{
        if (teaching.startsWith("G")
            || teaching.startsWith("H")
            || teaching.startsWith("I")
            || teaching.startsWith("K"))
        {
            return income * teachingHours
        }
        else
        {
            return income * teachingHours + 500000
        }
    }

    override fun showInfo() {
        println("Class: $teaching")
        println("Income per hour: $income")
        println("Teaching hours: $teachingHours")
        println("Actual income per month: ${incomeCalculator()}")
    }
}

fun main() {
    var teach = Teacher()
    teach.inputInfo()
    teach.showInfo()
}