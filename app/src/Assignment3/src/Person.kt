open class Person() {
    var name: String?= null
    var sex: String?= null
    var dob: String?= null
    var address:String?= null

    constructor(name:String,sex:String,dob:String,address:String):this(){
        this.name = name
        this.sex = sex
        this.dob = dob
        this.address = address
    }

    open fun inputInfo(){
        print("Enter name: ")
        name = readLine()!!
        print("Enter sex: ")
        sex = readLine()!!
        print("Enter dob: ")
        dob = readLine()!!
        print("Enter address: ")
        address = readLine()!!
    }

    open fun showInfo(){
        println("Name: $name")
        println("Sex: $sex")
        println("Date of birth: $dob")
        println("Address: $address")
    }
}


fun main(){
    var human = Person()
    human.inputInfo()
    human.showInfo()
}
