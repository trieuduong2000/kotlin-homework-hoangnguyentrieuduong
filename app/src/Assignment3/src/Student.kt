import java.util.regex.Pattern

class Student : Person() {
    var id: String=""
    var grade: Long=0
    var email: String=""
    var scholarship: Boolean=false
    override fun inputInfo() {
        print("Enter id: ")
        id = readLine()!!
        do {
            if (!id.startsWith("SV")) {
                print("Please re-enter the ID. It must begin with 'SV': ")
                id = readLine()!!
            }
            else
            {
                break
            }
        }while(true)
        print("Enter grade: ")
        grade = readLine()!!.toLong()
        do {
            if (grade>0 && grade<10)
            {
                scholarship=checkGrade(grade)
                break
            }
            else
            {
                print("Grade should be in the range of 0 to 10. Please re-enter: ")
                grade = readLine()!!.toLong()
            }
        }while(true)
        print("Enter email: ")
        email = readLine()!!
        do {
            if (!isEmailValid(email)){
                print("This is not an email. Please re-enter: ")
                email = readLine()!!
            }
            else break
        } while (true)
    }

    override fun showInfo() {
        println("ID: $id")
        println("Grade: $grade")
        println("Email: $email")
        println("Got scholarship? $scholarship")
    }

    fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    fun checkGrade(grade: Long): Boolean {
        if(grade >= 8)
        {
            return true
        }
        else
        {
            return false
        }
    }
}

fun main() {
    var stu = Student()
    stu.inputInfo()
    stu.showInfo()
}