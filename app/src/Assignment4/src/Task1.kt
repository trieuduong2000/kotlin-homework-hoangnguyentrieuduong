fun main() {
    val list = listOf(4,5,91,30,"Bread")
    print("Number of odds: " + findOddInList(list))
}

fun <T> findOddInList(list: List<T>): String {
    var count = 0
    for ( i in list.indices ) {
        val x = list[i]
        if (x is Int && x % 2 !=0)
        {
            count++
        }
    }
    return "$count"
}