fun <T> Array<T>.exchangeElement(posA: Int, posB: Int){
    this[posA] = this[posB].also{this[posB] = this[posA]}
}
fun main(){
    var arr1 = arrayOf(0,1,2,3,4,5,6,"test")
    println(arr1.joinToString(", "))
    println("Swapping element index 2 and 6:")
    arr1.exchangeElement(2,6)
    println(arr1.joinToString(", "))
}