fun <T : Comparable<T>> maximalElement(arr: Array<T>, begin: Int, end: Int): T {
    var max = arr[begin-1]

    for (i in begin until end) {
        if (arr[i]!! > max) {
            max = arr[i]
        }
    }
    return max
}

fun main(){
    var arr1 = arrayOf<Int>(0,4,6,7,1,2,3)
    println(maximalElement(arr1,1,7))
}